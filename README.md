**kraken-desktops**

*Source of Kraken OS debian metapackages - desktops , release 2018.10.02*

# PACKAGING

* Install package :

```sh
sudo apt install build-essential devscripts debhelper fakeroot
```

* Build .deb :

```sh
git clone https://gitlab.com/kraken-sec/pkg-deb/kraken-desktops.git
mkdir kraken-desktops/kraken-desktops-2018.10.02
cp -r kraken-desktops/debian/ kraken-desktops/kraken-desktops-2018.10.02/
cd kraken-desktops/kraken-desktops-2018.10.02/
```

```sh
debuild -i -uc -us --build=all
```

